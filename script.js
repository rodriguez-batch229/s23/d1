//[Section] - Objects
/* 
SYNTAX -> let/const objectName = {
    keyA: valueA,
    keyB: valueB
}
 */

console.log(typeof null);
let cellphone = {
    name: "Nokia 3210",
    manufactureDate: 1999,
};

console.log("Result from creating objects using initializersliteral notation");
console.log(cellphone);
console.log(typeof cellphone);

// this is an object

function Laptop(name, manufactureDate) {
    (this.name = name), (this.manufactureDate = manufactureDate);
}

// this is an instance  of an object
let laptop = new Laptop("Lenovo", 2008);
console.log("Result from creating objects using constructor");
console.log(laptop);

let myLaptop = new Laptop("Macbook Air", 2020);
console.log("Result from creating objects using constructor");
console.log(myLaptop);

// We cannot create new objects without the instance or the "new" keyword
let oldLaptop = Laptop("Portal R2E CCMC", 1980);
console.log("Result from creating objects using constructor");
console.log(oldLaptop); //return undefined value

// Creating empty objects
let computer = {};
console.log(computer);

let array = [laptop, myLaptop];

// this is confusing because of the array method
console.log(array[0]["name"]);
// proper way of accessing object propert value is by using dot notation
console.log(array[0].name);

// [Section] -Initializing, deleting, Re-assigning Object properties

let car = [];

// Adding key pairs
car.name = "Honda Civic";

console.log();

// We can create objects with

// {} - object literal greate for creating dynamic objects

// Contructor function - allow us to create objects with a defined structure

function Pokemon(name, type, level) {
    //this - keyword to refer to the object to be created with the constructor function
    this.name = name;
    this.type = type;
    this.level = level;
}
// "new" keyword allows us to create an instance of an object with a constructor function.

let pokemonInstance1 = new Pokemon("Bulbasaur", "grass", 32);

// Objects can have arrays as properties:

let professor1 = {
    name: "Romenick Garcia",
    age: 25,
    subjects: ["MongoDB", "HTML", "CSS", "JS"],
};
// We can access object properties with dot notation
console.log(professor1.subjects);

// what if we want to add an item in the array within the object

// Mini activity: Display in the console each subject from professor1 subjects array:

professor1.subjects.forEach((subject) => {
    console.log(subject);
});

let dog1 = {
    name: "Bantay",
    breed: "Golden Retriever",
};
let dog2 = {
    name: "Bolt",
    breed: "Aspin",
};

// Array of objects:
let dogs = [dog1, dog2];
console.log(dogs);

// delete object properties with "delete" keyword
//  Syntax -> delete onjectName.property

// Object method
// Functions is an object
// This is useful for creating functions that ar associated to a specific object

let person1 = {
    name: "Joji",
    talk: function () {
        console.log("hello");
    },
};

person1.talk();

// [Mini-Activity
//Create a new object as studentl
// The object should have the following properties:
//name, age, address
//Add 3 methods for studentl
// introduceName = which will display the studentl's name as:
// "Hello! My name is <nameofstudentl>"
// introduceAge = which will display the age of studentl as:
// "Hello! I am <ageofstudentl> years old. "
// introduceAddress = which will display the address of studentl as:
// "Hello! I am < nameofstudentl>. I live in <addressofstudentl>"

let student1 = {
    name: "Jose",
    age: 20,
    address: "Calamba",

    introduceName: function () {
        console.log(`Hello! My name is ${this.name}`);
    },
    introduceAge: function () {
        console.log(`Hello! I am ${this.age} years old`);
    },
    introduceAddress: function () {
        console.log(`Hello! I am ${this.name}. I live in ${this.address}`);
    },
};

student1.introduceName();
student1.introduceAge();
student1.introduceAddress();

function student(name, age, address) {
    this.name = name;
    this.age = age;
    this.address = address;

    this.introduceName = function () {
        console.log(`Hello! My name is ${this.name}`);
    };
    this.introduceAge = function () {
        console.log(`Hello! I am ${this.age} years old`);
    };
    this.introduceAddress = function () {
        console.log(`Hello! I am ${this.name}. I live in ${this.address}`);
    };
    // We can also methods that take other objects as an argument
    this.greet = function (person) {
        // person's properties are accessible in the method
        console.log(`Good day ${person.name}`);
    };
}

let newstudent1 = new student("TeeJae", 25, "Cainta");
console.log(newstudent1);

let newstudent2 = new student("Jose", 25, "Rizal");
newstudent2.greet(newstudent1);

// MiniActivity
// Create a new constructor function called Dog which is able to create objects with the following properties;
// name, breed
// the constructor function should also have 2 methods
// call() - is a method which will allow us to display the follwing message
// "Bark Bark Bark!"
// greet() - is a method which allow us to greet another person. this method should be able to receive an object which has a name property. upon invoking the greet() method it should be display
// "Bark Bark <nameofperson>"
// Create an instance/object from the dog constructor
// Invoke its call() method
// Invoke its greet() method to greet newstudent2

// Take Screenshot of your output and sent it to the hangouts

function Dog(name, breed) {
    this.name = name;
    this.breed = breed;

    this.call = function () {
        console.log("Bark Bark Bark!");
    };

    this.greet = function (person) {
        // hasOwnPropery() method will check if the object passed has the indicated property
        console.log(person.hasOwnProperty(`name`));
        console.log(`Bark Bark ${person.name}`);
    };
}

let newdog1 = new Dog("Aso", "Askal");

newdog1.call();
newdog1.greet(newstudent1);
newdog1.greet(newstudent2);

function sample1(sampleParameter) {
    // the argument you passed during invocation is now represented by the parameter
    console.log(sampleParameter.name);
    newdog1.name = "Gary";
}

// argument is the data passed during the function invocation
sample1(newdog1);
console.log(newdog1);
